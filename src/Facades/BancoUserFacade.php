<?php

namespace App\Facades;

use App\Form\TransactionFirstStepBancosType;
use App\Repository\BancosUserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use TransactionFirstStepBancosDto;

class BancoUserFacade
{
    /**
     * @var BancosUserRepository
     */
    private $bancosUserRepository;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(BancosUserRepository $bancosUserRepository, SerializerInterface $serializer)
    {
        $this->bancosUserRepository = $bancosUserRepository;
        $this->serializer = $serializer;
    }


    public function getBancosUserByResponse(Request $request)
    {
        return $this->serializer->deserialize($request->getContent(),TransactionFirstStepBancosDto::class, 'json');
    }

}