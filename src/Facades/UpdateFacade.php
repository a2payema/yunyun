<?php

namespace App\Facades;

class UpdateFacade
{
    public function UpdateUser()
    {
        try {
            $data = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($data);
            $em->flush();

            $this->addFlash(
                'success',
                'New Banco added!'
            );

            return $this->redirectToRoute('user_profile', ['id' => $this->getUser()->getId()]);
        } catch (\Exception $e) {

            $this->addFlash(
                'fail',
                'No new Transfer added!'
            );
            $logger->info(
                $e->getMessage(),
                [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage()
                ]
            );
        }
    }
}