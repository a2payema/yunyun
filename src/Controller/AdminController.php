<?php

namespace App\Controller;

use App\Entity\Bancos;
use App\Entity\Rate;
use App\Entity\Transaction;
use App\Factories\MonedaFactory;
use App\Form\RateType;
use App\Messages;
use App\Repository\RateRepository;
use App\Views\RatesView;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index()
    {
        $manager = $this->getDoctrine()->getManager();
        $transactions = $manager->getRepository(Transaction::class)
            ->findAllByStatusWithLimit(Transaction::STATUS_TYPE_EN_CURSO);

        return $this->render(
            'admin/index.html.twig',
            [
                'controller_name' => 'AdminController',
                'transactions' => $transactions
            ]
        );
    }

    /**
     * @Route("/admin/transactions", name="admin_transactions")
     */
    public function transactions()
    {
        $manager = $this->getDoctrine()->getManager();
        $transactions = $manager->getRepository(Transaction::class)
            ->findAllByStatus(Transaction::STATUS_TYPE_EN_CURSO);

        return $this->render(
            'admin/transactions.html.twig',
            [
                'controller_name' => 'AdminController',
                'transactions' => $transactions
            ]
        );
    }

    /**
     * @Route("/admin/transaction/{id}", name="admin_transaction", requirements={"id"="\d+"})
     */
    public function singleTransaction(Request $request, int $id)
    {
        $manager = $this->getDoctrine()->getManager();
        $transaction = $manager->getRepository(Transaction::class)->findOneBy(['id' => $id]);

        $form = $this->createFormBuilder()
            ->add('submitConfirm', SubmitType::class, ['label' => 'Confirm in transaction'])
            ->add('submitError', SubmitType::class, ['label' => 'Error in transaction'])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->getClickedButton()->getName() === 'submitError') {
                return $this->redirectToRoute('admin_transaction_error', ['id' => $id]);
            }

            if ($form->getClickedButton()->getName() === 'submitConfirm') {
                $transaction->setAdmin($this->getUser());
                $transaction->setClientStatus(Transaction::STATUS_TYPE_EN_FINALIZADA);
                $transaction->setAdminStatus(Transaction::ADMIN_STATUS_TYPE_DONE);
                $transaction->setDtcAdmin(new DateTime());

                $manager->persist($transaction);
                $manager->flush();

                $this->addFlash(Messages::SUCCESS_CSS_CLASS, 'Transacion confirmada!');
                return $this->redirectToRoute('admin_transactions');
            }

        }

        return $this->render(
            'admin/singleTransaction.html.twig',
            [
                'controller_name' => 'AdminController',
                'transaction' => $transaction,
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/admin/transaction/{id}/error", name="admin_transaction_error", requirements={"id"="\d+"})
     */
    public function singleTransactionError(Request $request, int $id)
    {
        $manager = $this->getDoctrine()->getManager();
        $transaction = $manager->getRepository(Transaction::class)->findOneBy(['id' => $id]);

        $form = $this->createFormBuilder()
            ->add(
                'confirmError',
                ChoiceType::class,
                [
                    'choices' => [
                        'Codigo de Verificacion equivocado' => Transaction::ERROR_TYPE_WRONG_CODE,
                        'Monto equivocado' => Transaction::ERROR_TYPE_WRONG_AMOUNT
                    ],
                    'label' => 'Confirm Error type'
                ]
            )
            ->add('submitError', SubmitType::class, ['label' => 'Error in transaction'])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();
            $transaction->setAdmin($this->getUser());
            $transaction->setClientStatus(Transaction::STATUS_TYPE_EN_CANCEL);
            $transaction->setAdminStatus(Transaction::ADMIN_STATUS_TYPE_ERROR_TYPE);
            $transaction->setErrorType($data['confirmError']);
            $transaction->setDtcAdmin(new DateTime());
            $manager->persist($transaction);
            $manager->flush();

            $this->addFlash(
                Messages::SUCCESS_CSS_CLASS,
                'Transacion confirmado error, sent notify to customer!'
            );
            return $this->redirectToRoute('admin_transactions');
        }

        return $this->render(
            'admin/singleTransactionError.html.twig',
            [
                'controller_name' => 'AdminController',
                'form' => $form->createView(),
                'transaction' => $transaction
            ]
        );
    }

    /**
     * @Route("/admin/rates", name="admin_rates")
     */
    public function rates(RateRepository $rateRepository)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $rates = $this->getRates($rateRepository);
        $ratesView = [];
        /**@var $rate Rate* */
        foreach ($rates as $rate) {
            $ratesView[] = new RatesView(
                $rate->getId(),
                MonedaFactory::generateMonedaById($rate->getMonedaBase()),
                MonedaFactory::generateMonedaById($rate->getMonedaCambio()),
                $rate->getRate(),
                $rate->getDtc()
            );
        }
        return $this->render(
            'admin/rates.html.twig',
            [
                'rates' => $ratesView
            ]
        );
    }

    /**
     * @Route("/admin/editRates/{rateId}", name="admin_edit_rates", requirements={"rateId"="\d+"})
     */
    public function editRates(int $rateId, Request $request, RateRepository $rateRepository)
    {
        $rate = $rateRepository->findOneBy(["id" => $rateId]);
        $form = $this->createForm(RateType::class, $rate, []);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $manager = $this->getDoctrine()->getManager();
            $data = $form->getData();
            $manager->persist($data);
            $manager->flush();
            $this->addFlash(
                Messages::SUCCESS_CSS_CLASS,
                'EditadoRate'
            );

            return $this->redirectToRoute('admin_rates');
        }

        return $this->render(
            'admin/editRates.html.twig',
            [
                'form' => $form->createView(),
                'monedaBase' => MonedaFactory::generateMonedaById($rate->getMonedaBase()),
                'monedaCambio' => MonedaFactory::generateMonedaById($rate->getMonedaCambio())
            ]
        );
    }

    /**@todo missing admin add rate **/

    private function getRates(RateRepository $rateRepository): array
    {
        $data = [];
        $data[] = $rateRepository->findOneBy(["monedaBase" => Bancos::MONEDA_DOLLAR], ["dtc" => "DESC"]);
        $data[] = $rateRepository->findOneBy(["monedaBase" => Bancos::MONEDA_REALES], ["dtc" => "DESC"]);
        return $data;
    }
}
