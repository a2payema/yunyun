<?php

namespace App\Controller;

use App\Deciders\FirstStepBancosDecider;
use App\Dtos\TransactionFirstStep\TransactionFirstStepBancosDto;
use App\Entity\BancosUser;
use App\Entity\Rate;
use App\Entity\Transaction;
use App\Entity\User;
use App\Factories\StatusFactory;
use App\Form\TransactionSteps\TransactionFirstStepBancosType;
use App\Messages;
use App\Repository\BancosUserRepository;
use App\Repository\RateRepository;
use App\Repository\TransactionRepository;
use App\Factories\MonedaFactory;
use App\Views\TransaccionesView;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Factories\TransactionFactory;

class TransactionController extends AbstractController
{
    /**
     * @Route("/transaction", name="transaction")
     */
    public function index(BancosUserRepository $er): Response
    {
        /**@var User $user */
        $user = $this->getUser();

        if (!$user->hasBancos() || !$user->hasMoreThanOneBanco()) {
            $this->addFlash(
                Messages::NOTICE_CSS_CLASS,
                'Usuario necesita bancos adicionados.'
            );
            return $this->redirectToRoute('home');
        }

        return $this->render('transaction/index.html.twig', []);
    }

    /**
     * @Route("/transaction/firstStep/bancos", name="transactionFirstStep", methods={"GET"})
     */
    public function getFirstStepBancosForm(BancosUserRepository $er): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $transactionFirstStepBancosDto = new TransactionFirstStepBancosDto($user->getBancosUsers());

        $formBancos = $this->createForm(TransactionFirstStepBancosType::class, $transactionFirstStepBancosDto, ['user' => $user]);
        $view = $this->render(
            'transaction/Forms/transactionFirstBancos.html.twig',
            [
                'form' => $formBancos->createView()
            ]
        );

        return $this->json(
            [
                'form' => $view
            ]
        );
    }

    /**
     * @Route("/transaction/firstStep/bancos", name="bancos_post_transaction", methods={"POST"})
     * @param BancosUserRepository $bancosUserRepository
     * @param RateRepository $rateRepository
     * @param FirstStepBancosDecider $firstStepBancosDecider
     * @param Request $request
     * @param LoggerInterface $logger
     * @return JsonResponse
     */
    public function postTransactionBancos(
        BancosUserRepository $bancosUserRepository,
        RateRepository $rateRepository,
        FirstStepBancosDecider $firstStepBancosDecider,
        Request $request,
        LoggerInterface $logger
    ) {
        $json = json_decode($request->getContent(), TRUE);
        if (!$firstStepBancosDecider->differentBancos($json)) {
            return $this->json(['error_message' => 'Mismo banco utilizado'], Response::HTTP_BAD_REQUEST);
        }
        try {

            /**@var BancosUser $bancoLlegada , $bancoSalida * */
            [$bancoLlegada, $bancoSalida] = $this->getBancos($bancosUserRepository, $json);

            if (empty($bancoLlegada) || empty($bancoSalida)) {
                return $this->json(['error_message' => 'Mismo banco utilizado'], Response::HTTP_NOT_FOUND);
            }

            /**@var Rate $rate * */
            $rate = $rateRepository->getLastByMonedas( $bancoSalida->getBancos()->getMoneda(),$bancoLlegada->getBancos()->getMoneda());
            return $this->json(
                [
                    'MonedaLlegada' => MonedaFactory::generateMonedaById($bancoLlegada->getBancos()->getMoneda()),
                    'MonedaSalida' => MonedaFactory::generateMonedaById($bancoSalida->getBancos()->getMoneda()),
                    'nombreDeBancoDeSalida' => $bancoSalida->getBancos()->getNombre(),
                    'nombreDeBancoDeLlegada' => $bancoLlegada->getBancos()->getNombre(),
                    'rate' => $rate->getRate()
                ],
                Response::HTTP_OK
            );

        } catch (\Exception $e) {

            $logger->error($e->getMessage());
            return $this->json(['transaction' => $request->getContent(), 'message' => 'Transaction invalid'], Response::HTTP_BAD_REQUEST);
        }
    }

    private function getBancos(BancosUserRepository $bancosUserRepository, array $json): array
    {
        return [
            $bancosUserRepository->find($json['transaction_first_step_bancos_bancoLlegada_banco']),
            $bancosUserRepository->find($json['transaction_first_step_bancos_bancoSalida_banco']),
        ];
    }

    /**
     * @Route("/transaction/postTransaction", name="post_transaction", methods={"POST"})
     */
    public function postTransaction(
        Request $request,
        TransactionFactory $transactionFactory,
        LoggerInterface $logger
    ): JsonResponse {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        /**@var User $user * */
        $user = $this->getUser();
        $json = json_decode($request->getContent(), TRUE, 512, JSON_THROW_ON_ERROR);

        try {
            $transaction = $transactionFactory->createTransactionFromJson($user, $json);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($transaction);
            $entityManager->flush();

        } catch (\Exception $e) {
            $logger->error($e->getMessage());
            return $this->json(['transaction' => $request->getContent(), 'message' => 'Transaction invalid'], Response::HTTP_BAD_REQUEST);
        }
        return $this->json(['id' => $transaction->getId(), 'message' => 'Transaction save correctly'], Response::HTTP_OK);
    }

    /**
     * @Route("/transaction/continue", name="continue_transaction", methods={"GET"})
     */
    public function continueTransaction()
    {
        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/transaction/history", name="history_transaction", methods={"GET"})
     */
    public function historyTransactions(
        StatusFactory $statusFactory,
        TransactionRepository $transactionRepository
    ) {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        /**@var User $user * */
        $user = $this->getUser();
        $transactionsViews = $this->getHistoryTransactions($statusFactory, $transactionRepository, $user);
        return $this->render(
            'transaction/historyTransaction.html.twig',
            [
                'transactionInProgress' => $transactionsViews
            ]
        );
    }

    private function getHistoryTransactions(StatusFactory $statusFactory, TransactionRepository $transactionRepository, User $user)
    {
        $transactions = $transactionRepository->findAllTransactions($user);
        $data = [];
        /**@var Transaction $transaction * */
        foreach ($transactions as $transaction) {
            $data[] = $this->createView($statusFactory, $transaction);
        }
        return $data;
    }

    private function createView(StatusFactory $statusFactory, Transaction $transaction)
    {
        return new TransaccionesView(
            $transaction->getId(),
            $transaction->getMontoSalida(),
            $transaction->getMontoLlegada(),
            $transaction->getBancoLlegada()->getNombre(),
            $transaction->getBancoSalida()->getNombre(),
            empty($transaction->getClientStatus()) ? 'N/A' : $statusFactory->getStatusForClient($transaction->getClientStatus()),
            MonedaFactory::generateMonedaById($transaction->getBancoLlegada()->getMoneda()),
            MonedaFactory::generateMonedaById($transaction->getBancoSalida()->getMoneda()),
            $transaction->getDtcUser()
        );
    }

    /**@todo work on restart transaction from previous state* */
}
