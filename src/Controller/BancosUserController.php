<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BancosUserController extends AbstractController
{
    /**
     * @Route("/bancosUser", name="bancos_user")
     */
    public function index(): Response
    {
        return $this->render('bancos_user/index.html.twig', [
            'controller_name' => 'BancosUserController',
        ]);
    }

    /**
     * @Route("/bancosUser/remove/{id}", name="bancos_user_remove", requirements={"id"="\d+"})
     */
    public function removeBancosUser()
    {

    }
}
