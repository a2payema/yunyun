<?php

namespace App\Controller;

use App\Entity\Transaction;
use App\Entity\User;
use App\Factories\MonedaFactory;
use App\Factories\StatusFactory;
use App\Messages;
use App\Form\RegistrationFormType;
use App\Repository\TransactionRepository;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserProfileController extends AbstractController
{
    /**
     * @Route("/user/profile/{id}", name="user_profile", requirements={"id"="\d+"})
     */
    public function index(
        int $id,
        Request $request,
        LoggerInterface $logger,
        TransactionRepository $transactionRepository,
        StatusFactory $statusFactory
    ) {
        $this->denyAccessUnlessGranted('ROLE_USER');
        /**@var User $user * */
        $user = $this->getUser();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try {
                $tempUser = $form->getData();
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($tempUser);
                $entityManager->flush();

                $this->addFlash(Messages::SUCCESS_CSS_CLASS, 'Updated Profile!');

                return $this->redirectToRoute('user_profile', ['id' => $id]);

            } catch (\Exception $e) {

                $this->addFlash(Messages::FAIL_CSS_CLASS, 'No update for profile!');
                $logger->info(
                    $e->getMessage(),
                    [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage()
                    ]
                );

                return $this->redirectToRoute('user_profile', ['id' => $id]);

            }
        }

        $transactions = $this->getLastTransactions($statusFactory, $transactionRepository, $user);
        return $this->render(
            'user_profile/index.html.twig',
            [
                'controller_name' => 'UserProfileController',
                'form' => $form->createView(),
                'user' => $this->getUser(),
                'transactions' => $transactions
            ]
        );
    }

    private function getLastTransactions(
        StatusFactory $statusFactory,
        TransactionRepository $transactionRepository,
        User $user
    ) {
        $transactions = $transactionRepository->findLastTransactions($user);
        $data = [];
        /**@var Transaction $transaction * */
        foreach ($transactions as $transaction) {
            $data[] = new \TransaccionesView(
                $transaction->getId(),
                $transaction->getMontoSalida(),
                $transaction->getMontoLlegada(),
                $transaction->getBancoLlegada()->getNombre(),
                $transaction->getBancoSalida()->getNombre(),
                $statusFactory->getStatusForClient($transaction->getClientStatus()),
                MonedaFactory::generateMonedaById($transaction->getBancoLlegada()->getMoneda()),
                MonedaFactory::generateMonedaById($transaction->getBancoSalida()->getMoneda()),
                $transaction->getDtcUser()
            );
        }
        return $data;
    }
}
