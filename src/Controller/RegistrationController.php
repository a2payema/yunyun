<?php

namespace App\Controller;

use App\Entity\BancosUser;
use App\Entity\User;
use App\Form\BancosUserType;
use App\Messages;
use App\Form\TransactionSteps\FirstStepBancosUserType;
use App\Form\RegistrationFormType;
use App\Repository\BancosUserRepository;
use App\Security\LoginFormAuthenticator;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/register", name="app_register")
     */
    public function register(
        Request                      $request,
        UserPasswordEncoderInterface $passwordEncoder,
        GuardAuthenticatorHandler    $guardHandler,
        LoginFormAuthenticator       $authenticator
    ): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('home');
        }

        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $user->setRoles(['ROLE_USER']);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            // do anything else you need here, like send an email

            return $guardHandler->authenticateUserAndHandleSuccess(
                $user,
                $request,
                $authenticator,
                'main' // firewall name in security.yaml
            );
        }
        return $this->render(
            'registration/register.html.twig',
            [
                'registrationForm' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/register/bancos", name="app_register_bancos")
     */
    public function registerBancosUsers(Request $request, LoggerInterface $logger, BancosUserRepository $bancosUserRepository): Response
    {
        $bancosUser = new BancosUser();
        $bancosUser->setUser($this->getUser());
        $bancosUsers = $bancosUserRepository->findBy(['user' => $this->getUser()]);

        $bancosUsed = [];
        if (!empty($bancosUsers)) {
            /**@var $bu BancosUser * */
            foreach ($bancosUsers as $bu) {
                $bancosUsed[] = $bu->getBancos()->getId();
            }
        }

        $form = empty($bancosUsed)
            ? $this->createForm(BancosUserType::class, $bancosUser)
            : $this->createForm(BancosUserType::class, $bancosUser, ['bancosUsed' => $bancosUsed]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $data = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($data);
                $em->flush();

                $this->addFlash(Messages::SUCCESS_CSS_CLASS, 'New Banco added!');

                return $this->redirectToRoute('user_profile', ['id' => $this->getUser()->getId()]);
            } catch (\Exception $e) {

                $this->addFlash(Messages::FAIL_CSS_CLASS, 'No new Banco added!');
                $logger->info($e->getMessage(), ['code' => $e->getCode(), 'message' => $e->getMessage()]);
            }
        }

        return $this->render(
            'registration/registerBancos.html.twig',
            [
                'registrationBancosForm' => $form->createView(),
            ]
        );
    }
}
