<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class VueExampleController extends AbstractController
{
    /**
     * @Route("/vue/example", name="vue_example")
     */
    public function index(): Response
    {
        $words = ['sky', 'cloud', 'wood', 'rock', 'forest',
            'mountain', 'breeze'];

        return $this->render('vue_example/index.html.twig', [
            'controller_name' => 'VueExampleController',
            'words'=>$words
        ]);
    }
}
