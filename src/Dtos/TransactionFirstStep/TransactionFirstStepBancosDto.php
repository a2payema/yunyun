<?php

namespace App\Dtos\TransactionFirstStep;

use App\Entity\BancosUser;
use Doctrine\Common\Collections\Collection;

class TransactionFirstStepBancosDto
{
    /**
     * @var Collection|BancosUser[]|null
     */
    private $bancoSalida;

    /**
     * @var Collection|BancosUser[]|null
     */
    private $bancoLlegada;

    /**
     * @param $bancos
     */
    public function __construct($bancos = null)
    {
        $this->bancoSalida = $bancos;
        $this->bancoLlegada = $bancos;
    }

    public function getBancoSalida()
    {
        return $this->bancoSalida;
    }

    public function setBancoSalida(array $bancoSalida)
    {
        $this->bancoSalida = $bancoSalida;
    }

    public function getBancoLlegada()
    {
        return $this->bancoLlegada;
    }

    public function setBancoLlegada(array $bancoLlegada)
    {
        $this->bancoLlegada = $bancoLlegada;
    }
}