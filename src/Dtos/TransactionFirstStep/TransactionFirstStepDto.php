<?php

namespace App\Dtos\TransactionFirstStep;

class TransactionFirstStepDto
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $userId;

    /**
     * @var TransactionFirstStepBancosDto
     */
    private $transactionBancos;

    /**
     * @var TransactionFirstStepMontosDto
     */
    private $transactionMontos;

    public function __construct(
        int $id,
        int $userId,
        TransactionFirstStepBancosDto $transactionBancos,
        TransactionFirstStepMontosDto $transactionMontos
    )
    {
        $this->id = $id;
        $this->userId = $userId;
        $this->transactionBancos = $transactionBancos;
        $this->transactionMontos = $transactionMontos;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setUserId(int $userId)
    {
        $this->userId = $userId;
    }

    public function getTransactionBancos(): TransactionFirstStepBancosDto
    {
        return $this->transactionBancos;
    }

    public function setTransactionBancos(TransactionFirstStepBancosDto $transactionBancos)
    {
        $this->transactionBancos = $transactionBancos;
    }

    public function getTransactionMontos(): TransactionFirstStepMontosDto
    {
        return $this->transactionMontos;
    }

    public function setTransactionMontos(TransactionFirstStepMontosDto $transactionMontos)
    {
        $this->transactionMontos = $transactionMontos;
    }


}