<?php

namespace App\Dtos\TransactionFirstStep;


class TransactionFirstStepMontosDto
{
    /**
     * @var float
     */
    private $montoSalida;

    /**
     * @var float
     */
    private $montoLlegada;

    /**
     * @var int
     */
    private $monedaSalida;

    /**
     * @var int
     */
    private $monedaLlegada;

    public function __construct(int $monedaSalida, int $monedaLlegada, float $montoSalida = 0, float $montoLlegada = 0)
    {
        $this->montoSalida = $montoSalida;
        $this->montoLlegada = $montoLlegada;
        $this->monedaSalida = $monedaSalida;
        $this->monedaLlegada = $monedaLlegada;
    }


    public function getMontoSalida(): float
    {
        return $this->montoSalida;
    }

    public function getMontoLlegada(): float
    {
        return $this->montoLlegada;
    }

    public function getMonedaSalida(): int
    {
        return $this->monedaSalida;
    }

    public function getMonedaLlegada(): int
    {
        return $this->monedaLlegada;
    }
}