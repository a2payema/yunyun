<?php

namespace App\Factories;

use App\Entity\Transaction;

class StatusFactory
{
    public function getStatusForClient(int $status): string
    {
        switch ($status) {
            case Transaction::STATUS_TYPE_EN_CURSO:
                return 'EN CURSO';
            case Transaction::STATUS_TYPE_EN_FINALIZADA:
                return 'FINALIZADA';
            case Transaction::STATUS_TYPE_EN_CANCEL:
                return 'CANCELADA';
        }
        return '';
    }
}