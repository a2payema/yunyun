<?php

namespace App\Factories;
use App\Entity\Bancos;

class MonedaFactory
{
    public static function generateMonedaById(int $monedaId): string
    {
        return ($monedaId === Bancos::MONEDA_DOLLAR) ? '$' : 'R$';
    }
}