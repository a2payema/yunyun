<?php

namespace App\Factories;

use App\Entity\Transaction;
use App\Entity\User;
use App\Repository\BancosUserRepository;
use DateTime;
use Ramsey\Uuid\Uuid;

class TransactionFactory
{
    /**
     * @var BancosUserRepository
     */
    private $bancosUserRepository;

    public function __construct(BancosUserRepository $bancosUserRepository)
    {
        $this->bancosUserRepository = $bancosUserRepository;
    }

    public function createTransactionFromJson(User $user, array $data): Transaction
    {
        $newTransaction = new Transaction();
        $newTransaction->setBancoSalida($this->bancosUserRepository->findOneBy(['id' => $data['bancoSalida']])->getBancos());
        $newTransaction->setBancoLlegada($this->bancosUserRepository->findOneBy(['id' => $data['bancoLlegada']])->getBancos());
        $newTransaction->setMontoSalida($data['valorDeSalida']);
        $newTransaction->setMontoLlegada($data['valorDeLlegada']);
        $newTransaction->setCodVerification($data['codigo']);
        $newTransaction->setUser($user);
        $newTransaction->setClientStatus(Transaction::STATUS_TYPE_EN_CURSO);
        $newTransaction->setDtcUser(new DateTime());
        return $newTransaction;
    }
}