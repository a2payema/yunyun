<?php

namespace App\Views;

use DateTimeInterface;

class TransaccionesView
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $montoDeSalida;

    /**
     * @var string
     */
    private $montoDeLlegada;

    /**
     * @var string
     */
    private $bancoDellegada;

    /**
     * @var string
     */
    private $bancoDeSalida;

    /**
     * @var DateTimeInterface
     */
    private $dtcUser;

    /**
     * @var string
     */
    private $status;

    /**
     * @var string
     */
    private $currencyDeLlegada;

    /**
     * @var string
     */
    private $currencyDeSalida;


    public function __construct(
        int $id,
        string $montoDeSalida,
        string $montoDeLlegada,
        string $bancoDellegada,
        string $bancoDeSalida,
        string $status,
        string $currencyDeLlegada,
        string $currencyDeSalida,
        DateTimeInterface $dtcUser
    )
    {
        $this->id = $id;
        $this->montoDeSalida = $montoDeSalida;
        $this->montoDeLlegada = $montoDeLlegada;
        $this->bancoDellegada = $bancoDellegada;
        $this->bancoDeSalida = $bancoDeSalida;
        $this->dtcUser = $dtcUser;
        $this->status = $status;
        $this->currencyDeLlegada = $currencyDeLlegada;
        $this->currencyDeSalida = $currencyDeSalida;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getMontoDeSalida(): string
    {
        return $this->montoDeSalida;
    }

    public function getMontoDeLlegada(): string
    {
        return $this->montoDeLlegada;
    }

    public function getBancoDellegada(): string
    {
        return $this->bancoDellegada;
    }

    public function getBancoDeSalida(): string
    {
        return $this->bancoDeSalida;
    }

    public function getDtcUser(): DateTimeInterface
    {
        return $this->dtcUser;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getCurrencyDeLlegada(): string
    {
        return $this->currencyDeLlegada;
    }

    public function getCurrencyDeSalida(): string
    {
        return $this->currencyDeSalida;
    }
}