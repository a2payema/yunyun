<?php
namespace App\Views;

use DateTimeInterface;

class RatesView
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $monedaBase;
    /**
     * @var string
     */
    private $monedaCambio;
    /**
     * @var float
     */
    private $rate;
    /**
     * @var DateTimeInterface
     */
    private $dtc;

    public function __construct(int $id, string $monedaBase, string $monedaCambio, float $rate, DateTimeInterface $dtc)
    {
        $this->id = $id;
        $this->monedaBase = $monedaBase;
        $this->monedaCambio = $monedaCambio;
        $this->rate = $rate;
        $this->dtc = $dtc;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    public function getMonedaBase(): string
    {
        return $this->monedaBase;
    }

    public function getMonedaCambio(): string
    {
        return $this->monedaCambio;
    }

    public function getRate(): float
    {
        return $this->rate;
    }

    public function getDtc(): DateTimeInterface
    {
        return $this->dtc;
    }
}