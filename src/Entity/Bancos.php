<?php

namespace App\Entity;

use App\Repository\BancosRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BancosRepository::class)
 */
class Bancos
{
    public const MONEDA_DOLLAR = 1;
    public const MONEDA_REALES = 2;
    public const MONEDA_PERUVIAN = 3;

    
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $nombre;

    /**
     * @ORM\Column(type="integer")
     */
    private $moneda;

    public function __construct()
    {
        $this->bancosUsers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getMoneda(): ?int
    {
        return $this->moneda;
    }

    public function setMoneda(int $moneda): self
    {
        $this->moneda = $moneda;

        return $this;
    }
}
