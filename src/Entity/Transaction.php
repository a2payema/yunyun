<?php

namespace App\Entity;

use App\Repository\TransactionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TransactionRepository::class)
 */
class Transaction
{
    // Status for client
    public const STATUS_TYPE_EN_CURSO = 1;
    public const STATUS_TYPE_EN_FINALIZADA = 2; 
    public const STATUS_TYPE_EN_CANCEL = 3;

    // Status for admin
    public const ADMIN_STATUS_TYPE_PENDING = 1;
    public const ADMIN_STATUS_TYPE_ERROR_TYPE = 2;
    public const ADMIN_STATUS_TYPE_DONE = 3;

    //Errors type
    public const ERROR_TYPE_WRONG_CODE = 1;
    public const ERROR_TYPE_WRONG_AMOUNT = 2;

    
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=true)
     */
    private $admin;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="transactions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Bancos::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $bancoSalida;

    /**
     * @ORM\ManyToOne(targetEntity=Bancos::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $bancoLlegada;

    /**
     * @ORM\Column(type="float")
     */
    private $montoSalida;

    /**
     * @ORM\Column(type="float")
     */
    private $montoLlegada;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $codVerification;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $clientStatus;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $adminStatus;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dtcAdmin;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dtcUser;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $errorType;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAdmin(): ?User
    {
        return $this->admin;
    }

    public function setAdmin(?User $admin): self
    {
        $this->admin = $admin;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getBancoSalida(): ?Bancos
    {
        return $this->bancoSalida;
    }

    public function setBancoSalida(?Bancos $bancoSalida): self
    {
        $this->bancoSalida = $bancoSalida;

        return $this;
    }

    public function getBancoLlegada(): ?Bancos
    {
        return $this->bancoLlegada;
    }

    public function setBancoLlegada(?Bancos $bancoLlegada): self
    {
        $this->bancoLlegada = $bancoLlegada;

        return $this;
    }

    public function getMontoSalida(): ?float
    {
        return $this->montoSalida;
    }

    public function setMontoSalida(float $montoSalida): self
    {
        $this->montoSalida = $montoSalida;

        return $this;
    }

    public function getMontoLlegada(): ?float
    {
        return $this->montoLlegada;
    }

    public function setMontoLlegada(float $montoLlegada): self
    {
        $this->montoLlegada = $montoLlegada;

        return $this;
    }

    public function getCodVerification(): ?string
    {
        return $this->codVerification;
    }

    public function setCodVerification(string $codVerification): self
    {
        $this->codVerification = $codVerification;

        return $this;
    }

    public function getClientStatus(): ?int
    {
        return $this->clientStatus;
    }

    public function setClientStatus(int $clientStatus): self
    {
        $this->clientStatus = $clientStatus;

        return $this;
    }

    public function getAdminStatus(): ?int
    {
        return $this->adminStatus;
    }

    public function setAdminStatus(int $adminStatus): self
    {
        $this->adminStatus = $adminStatus;

        return $this;
    }

    public function getDtcAdmin(): ?\DateTimeInterface
    {
        return $this->dtcAdmin;
    }

    public function setDtcAdmin(?\DateTimeInterface $dtcAdmin): self
    {
        $this->dtcAdmin = $dtcAdmin;

        return $this;
    }

    public function getDtcUser(): ?\DateTimeInterface
    {
        return $this->dtcUser;
    }

    public function setDtcUser(\DateTimeInterface $dtcUser): self
    {
        $this->dtcUser = $dtcUser;

        return $this;
    }

    public function getErrorType(): ?int
    {
        return $this->errorType;
    }

    public function setErrorType(?int $errorType): self
    {
        $this->errorType = $errorType;

        return $this;
    }
}
