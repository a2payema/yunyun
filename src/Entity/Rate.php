<?php

namespace App\Entity;

use App\Repository\RateRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RateRepository::class)
 */
class Rate
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $monedaBase;

    /**
     * @ORM\Column(type="integer")
     */
    private $monedaCambio;

    /**
     * @ORM\Column(type="float")
     */
    private $rate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dtc;

    public function __construct($id, $monedaBase, $monedaCambio, $rate, $dtc)
    {
        $this->id = $id;
        $this->monedaBase = $monedaBase;
        $this->monedaCambio = $monedaCambio;
        $this->rate = $rate;
        $this->dtc = $dtc;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMonedaBase(): ?int
    {
        return $this->monedaBase;
    }

    public function setMonedaBase(int $MonedaBase): self
    {
        $this->monedaBase = $MonedaBase;

        return $this;
    }

    public function getMonedaCambio(): ?int
    {
        return $this->monedaCambio;
    }

    public function setMonedaCambio(int $MonedaCambio): self
    {
        $this->monedaCambio = $MonedaCambio;

        return $this;
    }

    public function getRate(): ?float
    {
        return $this->rate;
    }

    public function setRate(float $rate): self
    {
        $this->rate = $rate;

        return $this;
    }

    public function getDtc(): ?\DateTimeInterface
    {
        return $this->dtc;
    }

    public function setDtc(\DateTimeInterface $dtc): self
    {
        $this->dtc = $dtc;

        return $this;
    }
}
