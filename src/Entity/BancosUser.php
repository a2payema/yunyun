<?php

namespace App\Entity;

use App\Repository\BancosUserRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BancosUserRepository::class)
 */
class BancosUser
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=120)
     */
    private $cuenta;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="bancosUsers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     */
    private $alias;

    /**
     * @ORM\ManyToOne(targetEntity=Bancos::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $bancos;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCuenta(): ?string
    {
        return $this->cuenta;
    }

    public function setCuenta(string $cuenta): self
    {
        $this->cuenta = $cuenta;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getAlias(): ?string
    {
        return $this->alias;
    }

    public function setAlias(string $alias): void
    {
        $this->alias = $alias;
    }

    public function getBancos(): ?Bancos
    {
        return $this->bancos;
    }

    public function setBancos(?Bancos $bancos): self
    {
        $this->bancos = $bancos;

        return $this;
    }

    public function getBancosName(): string
    {
        return $this->getBancos()->getNombre();
    }
}
