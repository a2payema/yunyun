<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface
{
    public const LEGAL_DOCUMENT_PASSPORT= 'PASSPORT';
    public const LEGAL_DOCUMENT_DNI= 'DNI';
    public const LEGAL_DOCUMENT_TRANSIT_PERMIT= 'PERMISO DE CONDUCCION';
    public const LEGAL_DOCUMENT_FOREIGN_PERMIT= 'PERMISO DE EXTRANJERO';

    public const LEGAL_DOCUMENTS = [
        'DNI'=>self::LEGAL_DOCUMENT_DNI,
        'PASSPORT'=>self::LEGAL_DOCUMENT_PASSPORT,
        'PERMISO DE CONDUCCION'=>self::LEGAL_DOCUMENT_TRANSIT_PERMIT,
        'PERMISO DE EXTRANJERO'=>self::LEGAL_DOCUMENT_FOREIGN_PERMIT
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $legalDocument;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $legalDocumentNumber;

    /**
     * @ORM\OneToMany(targetEntity=BancosUser::class, mappedBy="user")
     */
    private $bancosUsers;

    /**
     * @ORM\OneToMany(targetEntity=Transaction::class, mappedBy="user")
     */
    private $transactions;

    public function __construct()
    {
        $this->bancosUsers = new ArrayCollection();
        $this->transactions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getLegalDocument(): ?string
    {
        return $this->legalDocument;
    }

    public function setLegalDocument(?string $legalDocument): self
    {
        if(!in_array($legalDocument, self::LEGAL_DOCUMENTS, TRUE)){
            throw new \InvalidArgumentException("Invalid legal Document");
        }

        $this->legalDocument = $legalDocument;

        return $this;
    }

    public function getLegalDocumentNumber(): ?string
    {
        return $this->legalDocumentNumber;
    }

    public function setLegalDocumentNumber(?string $legalDocumentNumber): self
    {
        $this->legalDocumentNumber = $legalDocumentNumber;

        return $this;
    }

    /**
     * @return Collection|BancosUser[]
     */
    public function getBancosUsers(): Collection
    {
        return $this->bancosUsers;
    }

    public function hasBancos(): bool
    {
        return !$this->bancosUsers->isEmpty();
    }

    public function hasMoreThanOneBanco(): bool
    {
        return $this->bancosUsers->count() > 1;
    }

    public function lessThatOneBancoUsers(): bool
    {
        return count($this->bancosUsers->toArray()) <= 1;
    }

    public function addBancosUser(BancosUser $bancosUser): self
    {
        if (!$this->bancosUsers->contains($bancosUser)) {
            $this->bancosUsers[] = $bancosUser;
            $bancosUser->setUser($this);
        }

        return $this;
    }

    public function removeBancosUser(BancosUser $bancosUser): self
    {
        if ($this->bancosUsers->contains($bancosUser)) {
            $this->bancosUsers->removeElement($bancosUser);
            // set the owning side to null (unless already changed)
            if ($bancosUser->getUser() === $this) {
                $bancosUser->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Transaction[]
     */
    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    public function addTransaction(Transaction $transaction): self
    {
        if (!$this->transactions->contains($transaction)) {
            $this->transactions[] = $transaction;
            $transaction->setUser($this);
        }

        return $this;
    }

    public function removeTransaction(Transaction $transaction): self
    {
        if ($this->transactions->contains($transaction)) {
            $this->transactions->removeElement($transaction);
            // set the owning side to null (unless already changed)
            if ($transaction->getUser() === $this) {
                $transaction->setUser(null);
            }
        }

        return $this;
    }

    public function hasTransactions(): bool
    {
        return !$this->transactions->isEmpty();
    }
}
