<?php

namespace App\Repository;

use App\Entity\BancosUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BancosUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method BancosUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method BancosUser[]    findAll()
 * @method BancosUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BancosUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BancosUser::class);
    }

    // /**
    //  * @return BancosUser[] Returns an array of BancosUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BancosUser
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
