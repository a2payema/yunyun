<?php

namespace App\Repository;

use App\Entity\Transaction;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Transaction|null find($id, $lockMode = NULL, $lockVersion = NULL)
 * @method Transaction|null findOneBy(array $criteria, array $orderBy = NULL)
 * @method Transaction[]    findAll()
 * @method Transaction[]    findBy(array $criteria, array $orderBy = NULL, $limit = NULL, $offset = NULL)
 */
class TransactionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Transaction::class);
    }

    public function findAllByStatus(int $value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.clientStatus =:clientStatus')
            ->setParameter('clientStatus', $value)
            ->orderBy('t.codVerification', 'ASC')
            ->getQuery()
            ->getResult();
    }
    public function findLastTransactions(User $user)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.user =:user')
            ->setParameter('user', $user)
            ->orderBy('t.codVerification', 'ASC')
            ->setMaxResults(10)
            ->orderBy('t.dtcUser', "DESC")
            ->getQuery()
            ->getResult();
    }

    public function findAllTransactions(User $user)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.user =:user')
            ->setParameter('user', $user)
            ->orderBy('t.dtcUser', "DESC")
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Transaction[] Returns an array of Transaction objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Transaction
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findAllByStatusWithLimit(int $value,int $num =5)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.clientStatus =:clientStatus')
            ->setParameter('clientStatus', $value)
            ->orderBy('t.codVerification', 'ASC')
            ->setMaxResults($num)
            ->getQuery()
            ->getResult();
    }
}
