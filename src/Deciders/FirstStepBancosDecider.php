<?php

namespace App\Deciders;

class FirstStepBancosDecider
{
    public function differentBancos(array $bancos): bool
    {
        return $bancos['transaction_first_step_bancos_bancoLlegada_banco'] !== $bancos['transaction_first_step_bancos_bancoSalida_banco'];
    }
}