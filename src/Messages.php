<?php

namespace App;

class Messages
{
    public const FAIL_CSS_CLASS = 'danger';
    public const NOTICE_CSS_CLASS = 'warning';
    public const SUCCESS_CSS_CLASS = 'success';
}