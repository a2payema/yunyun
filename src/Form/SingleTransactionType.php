<?php

namespace App\Form;

use App\Entity\Transaction;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SingleTransactionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('montoSalida', TextType::class)
            ->add('montoLlegada',TextType::class)
            ->add('codVerification',TextType::class)
            ->add('clientStatus', HiddenType::class)
            ->add('adminStatus', HiddenType::class)
            ->add('dtcAdmin',DateTimeType::class)
            ->add('dtcUser',DateTimeType::class)
            ->add('admin',HiddenType::class)
            ->add('user', TextType::class)
            ->add('bancoSalida',TextType::class)
            ->add('bancoLlegada',TextType::class)

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Transaction::class,
        ]);
    }
}
