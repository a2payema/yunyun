<?php

namespace App\Form\TransactionSteps;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Dtos\TransactionFirstStep\TransactionFirstStepBancosDto;

class TransactionFirstStepBancosType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $options['user'] ?: NULL;
        $builder
            ->add(
                'bancoSalida',
                FirstStepBancosUserType::class,['user'=>$user]
            )
            ->add(
                'bancoLlegada',
                FirstStepBancosUserType::class,['user'=>$user]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => TransactionFirstStepBancosDto::class,
                'user'=> null
            ]
        );
    }
}
