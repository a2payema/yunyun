<?php

namespace App\Form\TransactionSteps;

use App\Entity\BancosUser;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FirstStepBancosUserType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $options['user'];
        $builder
            ->add(
                'banco',
                EntityType::class,
                [
                    'placeholder' => 'Elige una opcion',
                    'class' => BancosUser::class,
                    'query_builder'=> function(EntityRepository $er)use ($user){
                        return $er->createQueryBuilder('b')
                            ->where('b.user = :user')
                            ->setParameter('user',$user );
                    },
                    'choice_label'=>function($bancosUser){
                        return $bancosUser->getBancosName();
                    }
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => null,
                'user'=> null
            ]
        );
    }
}