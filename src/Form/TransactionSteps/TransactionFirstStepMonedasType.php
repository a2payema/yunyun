<?php

namespace App\Form\TransactionSteps;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Dtos\TransactionFirstStep\TransactionFirstStepMontosDto;

class TransactionFirstStepMonedasType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'montoSalida',
                TextType::class
            )
            ->add(
                'montoLlegada',
                TextType::class
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => TransactionFirstStepMontosDto::class
            ]
        );
    }
}
