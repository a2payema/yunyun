<?php

namespace App\Form;

use App\Entity\Bancos;
use App\Entity\BancosUser;
use App\Repository\BancosRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BancosUserType extends AbstractType
{
    private $bancosUsed;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->bancosUsed = empty($options['bancosUsed']) ? [] : $options['bancosUsed'];
        $builder
            ->add('cuenta', TextType::class)
            ->add('alias', TextType::class)
            ->add(
                'bancos',
                EntityType::class,
                [
                    'class' => Bancos::class,
                    'choice_label' => 'nombre',
                    'query_builder' => function (BancosRepository $er) {
                        return empty($this->bancosUsed)
                            ? $er->createQueryBuilder('b')
                                ->orderBy('b.id', 'ASC')
                            : $er->createQueryBuilder('b')
                                ->where('b.id NOT IN(:bancosUsed)')
                                ->setParameter('bancosUsed', $this->bancosUsed);
                    }
                ]
            )->add('submit', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => BancosUser::class,
                'bancosUsed' => NULL
            ]
        );
    }
}
