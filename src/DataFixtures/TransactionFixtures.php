<?php

namespace App\DataFixtures;

use App\Entity\Bancos;
use App\Entity\Transaction;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class TransactionFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $user = $manager->getRepository(User::class)->findOneBy(['email' => 'test1@gmail.com']);
        $bancoSalida = $manager->getRepository(Bancos::class)->findOneBy(['nombre' => 'BBVA']);
        $bancoLlegada = $manager->getRepository(Bancos::class)->findOneBy(['nombre' => 'Transferwise']);

        $transaction1 = new Transaction();
        $transaction2 = new Transaction();
        $transaction3 = new Transaction();
        $transaction4 = new Transaction();
        $transaction5 = new Transaction();

        $transactions = [$transaction1, $transaction2, $transaction3, $transaction4, $transaction5];

        $i=0;
        foreach ($transactions as $transaction) {
            $transaction->setUser($user);
            $transaction->setBancoSalida($bancoSalida);
            $transaction->setBancoLlegada($bancoLlegada);
            $transaction->setClientStatus(Transaction::STATUS_TYPE_EN_CURSO);
            $transaction->setAdminStatus(Transaction::ADMIN_STATUS_TYPE_PENDING);
            $transaction->setDtcUser(new \DateTime());
            $transaction->setMontoSalida(100);
            $transaction->setMontoLlegada(18.43);
            $transaction->setCodVerification(sprintf('0000%s',$i));
            $manager->persist($transaction);
            $i++;
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            BancosFixtures::class
        ];
    }
}
