<?php

namespace App\DataFixtures;

use App\Entity\Bancos;
use App\Entity\BancosUser;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class BancosFixtures extends Fixture implements DependentFixtureInterface
{
    /** @var User */
    private $user;

    public function load(ObjectManager $manager)
    {

        $user = $manager->getRepository(User::class)->findOneBy(['email'=>'test1@gmail.com']);
        $banco = new Bancos();
        $banco->setNombre('BBVA');
        $banco->setMoneda(Bancos::MONEDA_REALES);

        $banco2 = new Bancos();
        $banco2->setNombre('Transferwise');
        $banco2->setMoneda(Bancos::MONEDA_DOLLAR);

        $bancoUser = new BancosUser();
        $bancoUser->setBancos($banco);
        $bancoUser->setUser($user);
        $bancoUser->setCuenta('000-0000-0000');

        $bancoUser2 = new BancosUser();
        $bancoUser2->setBancos($banco2);
        $bancoUser2->setUser($user);
        $bancoUser2->setCuenta('000-0000-0001');

        $banco->addBancosUser($bancoUser);
        $banco2->addBancosUser($bancoUser2);

        $manager->persist($bancoUser);
        $manager->persist($bancoUser2);
        $manager->persist($banco);
        $manager->persist($banco2);
        $manager->persist($user);
        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
        );
    }
}
