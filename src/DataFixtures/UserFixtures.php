<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $user1 = new User();
        $user1->setEmail('test1@gmail.com');
        $user1->setName('test');
        $user1->setLastname('case');
        $user1->setRoles(['ROLE_USER']);
        $user1->setPassword(
            $this->passwordEncoder->encodePassword(
                $user1,
                '123456'
            )
        );

        $user2 = new User();
        $user2->setEmail('a2payema.work@gmail.com');
        $user2->setName('Andres');
        $user2->setLastname('Payema');
        $user2->setRoles(['ROLE_ADMIN']);
        $user2->setPassword(
            $this->passwordEncoder->encodePassword(
                $user2,
                '123456'
            )
        );

        foreach ([$user1,$user2] as $user){
            $manager->persist($user);
        }

        $manager->flush();
    }
}
