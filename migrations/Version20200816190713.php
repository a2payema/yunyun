<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200816190713 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE bancos (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(100) NOT NULL, moneda INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bancos_user (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, bancos_id INT NOT NULL, cuenta VARCHAR(120) NOT NULL, INDEX IDX_71031E19A76ED395 (user_id), INDEX IDX_71031E1926DCA429 (bancos_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE transaction (id INT AUTO_INCREMENT NOT NULL, admin_id INT DEFAULT NULL, user_id INT NOT NULL, banco_salida_id INT NOT NULL, banco_llegada_id INT NOT NULL, monto_salida DOUBLE PRECISION NOT NULL, monto_llegada DOUBLE PRECISION NOT NULL, cod_verification VARCHAR(100) NOT NULL, client_status INT NOT NULL, admin_status INT NOT NULL, dtc_admin DATETIME DEFAULT NULL, dtc_user DATETIME NOT NULL, INDEX IDX_723705D1642B8210 (admin_id), INDEX IDX_723705D1A76ED395 (user_id), INDEX IDX_723705D1CCF93418 (banco_salida_id), INDEX IDX_723705D1FE7600D4 (banco_llegada_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE bancos_user ADD CONSTRAINT FK_71031E19A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE bancos_user ADD CONSTRAINT FK_71031E1926DCA429 FOREIGN KEY (bancos_id) REFERENCES bancos (id)');
        $this->addSql('ALTER TABLE transaction ADD CONSTRAINT FK_723705D1642B8210 FOREIGN KEY (admin_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE transaction ADD CONSTRAINT FK_723705D1A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE transaction ADD CONSTRAINT FK_723705D1CCF93418 FOREIGN KEY (banco_salida_id) REFERENCES bancos (id)');
        $this->addSql('ALTER TABLE transaction ADD CONSTRAINT FK_723705D1FE7600D4 FOREIGN KEY (banco_llegada_id) REFERENCES bancos (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE bancos_user DROP FOREIGN KEY FK_71031E1926DCA429');
        $this->addSql('ALTER TABLE transaction DROP FOREIGN KEY FK_723705D1CCF93418');
        $this->addSql('ALTER TABLE transaction DROP FOREIGN KEY FK_723705D1FE7600D4');
        $this->addSql('DROP TABLE bancos');
        $this->addSql('DROP TABLE bancos_user');
        $this->addSql('DROP TABLE transaction');
    }
}
