import Vue from 'vue';
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export const store = new Vuex.Store({
    state: {
        transactionCurrentStep: "TransactionFirstStep",
        transactionInfo: [],
        errorFound: '',
        stepCompletes: [
            {'firstStep': true},
            {'secondStep': false},
            {'thirdStep': false},
        ]
    },
    getters: {
        getTransactionCurrentStep: state => state.transactionCurrentStep,
        getTransactionInfo: state => state.transactionInfo,
        getTransactionInfoAsJson: state => {
            return {
                "bancoSalida": state.transactionInfo["bancoSalida"],
                "bancoLlegada": state.transactionInfo["bancoLlegada"],
                "valorDeSalida": state.transactionInfo["valorDeSalida"],
                "valorDeLlegada": state.transactionInfo["valorDeLlegada"],
                "codigo": state.transactionInfo["codigo"]
            }
        },
        getErrorMsg: state => state.errorFound,
        getStep: (state) => {
            switch ([state.stepCompletes['firstStep'], state.stepCompletes['secondStep'], state.stepCompletes['thirdStep']]) {
                case [true, true, false]:
                    return 'secondStep'
                case [true, true, true]:
                    return 'thirdStep'
                default:
                    return 'firstStep'
            }
        }
    },
    mutations: {
        addTransaction: (state, transactionInfo) => state.transactionInfo = transactionInfo,
        addVerifyCod: (state, codigo) => {
            state.transactionInfo['codigo'] = codigo;
        },
        clearTransaction: (state) => {
            state.transactionInfo = []
        },
        setError: (state, errorMsg) => {
            state.errorFound = errorMsg
        }
    },
    actions: {
        postTransaction: async ({commit, getters}) => {
            await axios.post('/transaction/postTransaction', getters.getTransactionInfoAsJson)
                .then((response) => {
                    return response
                })
                .catch(function (error) {
                    return error
                })
        },
        getRates: async ()=>{
            await axios.get('/rates')
                .then((response) => {
                    return response
                })
                .catch(function (error) {
                    return error
                })
        }

    }
})