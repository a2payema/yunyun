import Example from "./components/Example";
import About from "./components/About";
import Home from "./components/Home";
import Transaction from "./components/Transaction";

export const routes = [
    {path: '/', component: Home},
    {path: '/vue/example', component: Example},
    {path: '/about', component: About},
    {path: '/transaction', component: Transaction}
];