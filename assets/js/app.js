// app.js
import '../css/app.css';
import './vanilla';

/*Vue set up*/
import Vue from 'vue';
import VueRouter from 'vue-router';
import {routes} from './Vue/routes.js';
import {store} from './Vue/Store/store'

/*Bootstrap*/
const $ = require('jquery');
require('bootstrap/dist/css/bootstrap.css');
require('bootstrap/dist/js/bootstrap.js');
require('@fortawesome/fontawesome-free/css/all.min.css');
require('@fortawesome/fontawesome-free/js/all.js');

import App from './Vue/App';

Vue.use(VueRouter);

const router = new VueRouter({
    routes,
    mode: 'history',
    scrollBehavior()
    {
        return {x: 0, y: 0};
    }
});

new Vue({
    router,
    store,
    el: '#app',
    render: h => h(App)
});

$(document).ready(function() {
    $('[data-toggle="popover"]').popover();
});